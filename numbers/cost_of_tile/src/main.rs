/*
 * Projects.rs
 * Filename: ./numbers/cost_of_tile/src/main.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


//! Find cost of covering a W x H tile area
//! where each tile is an arbitrary amount of dollars.

extern crate clap;

fn main() -> Result<(), Box<dyn (::std::error::Error)>> {
    use clap::{App, Arg};
    let matches = App::new("cost_of_tile")
        .author("Anirudh Balaji <anirudhqazxswced@gmail.com>")
        .about("Computes the cost of covering a W x H tile area using a cost entered by the user.")
        .arg(Arg::with_name("cost")
             .takes_value(true)
             .required(true)
             .help("The cost of each tile in dollars.")
             .index(1))
        .arg(Arg::with_name("width")
             .takes_value(true)
             .required(true)
             .help("The width of the tile area in units.")
             .index(2))
        .arg(Arg::with_name("height")
             .takes_value(true)
             .required(true)
             .help("The height of the tile area in units.")
             .index(3))
        .get_matches();
    let cost_per_tile = matches.value_of("cost").unwrap().parse::<f64>()?;
    let width = matches.value_of("width").unwrap().parse::<f64>()?;
    let height = matches.value_of("height").unwrap().parse::<f64>()?;
    println!("Cost: ${}", cost_per_tile*width*height);
    Ok(())
}
