/*
 * Projects.rs
 * Filename: ./numbers/pi/src/main.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


//! Approximation of Pi
//! using the Madhava-Leibniz algorithm
//!
//! It is one of the earliest algorithms
//! found.
//!
//! Note: only about 25 iterations are required
//! before the Rust floating point type
//! runs out of precision to store pi.

// Argument parsing
extern crate clap;

// Sum:
// pi = sqrt(12) * sum(k=0->inf, -3^-k / 2k+1)
fn approximate_pi(iters: u32) -> f64 {
    let sqrt_12 = 12f64.sqrt();
    let mut sum = 0f64;
    for k in 0..iters {
        let neg_3_neg_k = (-3f64).powi(-(k as i32));
        let two_k_plus_1 = (2f64*k as f64)+1f64;
        sum += neg_3_neg_k / two_k_plus_1;
    }
    sum*sqrt_12
}

fn main() -> Result<(), Box<dyn (::std::error::Error)>> {
    use clap::{App, Arg};
    let matches = App::new("pi")
        .author("Anirudh Balaji <anirudhqazxswced@gmail.com>")
        .about("Calculates pi using the Madhava-Leibniz algorithm.")
        .arg(Arg::with_name("n_iters")
             .required(true)
             .help("Number of iterations to run the algorithm for.\nNote: after about 25 iterations, the Rust floating point type will have no more precision.")
             .takes_value(true)
             .index(1))
        .get_matches();
    use std::time::Instant;
    let n_iters = matches.value_of("n_iters").unwrap().parse()?; // It is a required value
    // Time it.
    let now = Instant::now();
    let approx_pi = approximate_pi(n_iters);
    let elapsed = now.elapsed();
    // Convert to seconds + subsecs
    let elapsed = elapsed.as_secs() as f64 + elapsed.subsec_nanos() as f64 * 1e-9;
    println!("Approximation of pi imperatively in {}s ({} iterations): {}", elapsed, n_iters, approx_pi);
    Ok(())
}
