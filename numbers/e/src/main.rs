/*
 * Projects.rs
 * Filename: ./numbers/e/src/main.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


//! Approximation of e
//!
//! Uses the actual definition of e.

extern crate clap;

fn main() -> Result<(), Box<dyn (::std::error::Error)>> {
    use clap::{App, Arg};
    let matches = App::new("e")
        .author("Anirudh Balaji <anirudhqazxswced@gmail.com>")
        .about("Approximates e.")
        .arg(Arg::with_name("n_iters")
             .required(true)
             .takes_value(true)
             .help("Number of iterations.\nAround 10,000 iterations, Rust's floating point type runs out of precision.")
             .index(1))
        .get_matches();
    let n_iters = matches.value_of("n_iters").unwrap().parse()?;
    // e = lim n->inf (1 + 1/n)^n
    let e = (1f64 + (1f64 / n_iters as f64)).powf(n_iters);
    println!("e (n={}): {}", n_iters, e);
    Ok(())
}
