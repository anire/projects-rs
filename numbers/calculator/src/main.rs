/*
 * Projects.rs
 * Filename: ./numbers/calculator/src/main.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


//! Reverse polish notation calculator.
//!
//! Algorithms:
//! nth root algorithm (A to the nTh root) {
//!     let x = <guess>;
//!     until x desired precision (p. 1000 iterations) {
//!         x += (1/n) * (A/x^(b-1) - x);
//!     }
//!     return x;
//! }
//!
//! euclidean modulo (a mod b) {
//!     let r = a rem b (p. a%b);
//!     if r < 0 {
//!         return r + |b| (p. b.abs());
//!     } else {
//!         return r;
//!     }
//! }

use std::collections::VecDeque;
use std::io::{self, prelude::*};

fn main() -> io::Result<()> {
    let mut stack = VecDeque::new();
    let stdin = io::stdin();
    let mut stdin = stdin.lock();
    let mut stdout = io::stdout();
    let mut line = String::new();
    'main: loop { // heh, "main loop"
        print!("> ");
        stdout.flush()?;
        line.clear();
        stdin.read_line(&mut line)?;
        // trim line
        let trimmed = line.trim().to_owned();
        line = trimmed;
        if let Ok(n) = line.parse() {
            handle_num(&mut stack, n);
        } else {
            if handle_command(&mut stack, &line) {
                break 'main;
            }
        }
    }
    Ok(())
}

fn handle_num(s: &mut VecDeque<f64>, n: f64) {
    s.push_front(n);
}

fn handle_command(s: &mut VecDeque<f64>, cmd: &str) -> bool {
    match cmd {
        "quit" | "exit" => true,
        "help" => {
            // Give help
            println!("? = inspect stack");
            println!("<-> = transpose first two items on stack");
            println!("+ - * / = add / subtract / multiply / divide");
            println!("^ = power");
            println!("rem mod = remainder / euclidean modulo");
            println!("rootn logn = nth root / log n");
            println!("sqrt cbrt = square root / cube root");
            println!("^2 ^3 = squared / cubed");
            println!("log2 log10 = log 2 / log 10");
            println!("ln = natural logarithm");
            println!("x = trash top item of stack");
            println!("pi & e constants supported");
            println!("Note: numbers are popped to the front of the stack.");
            println!("First operand is first number on stack, second is second number on stack.");
            false
        },
        "?" => {
            // inspect stack
            for item in s.iter() {
                println!("{}", item);
            }
            false
        },
        "<->" => {
            // transpose first two items
            if let Some(a) = s.pop_front() {
                if let Some(b) = s.pop_front() {
                    s.push_front(a);
                    s.push_front(b);
                    false
                } else {
                    println!("Only got one operand, pushing back on stack");
                    s.push_front(a);
                    false
                }
            } else {
                println!("Empty stack!");
                false
            }
        },
        o @ "+" | o @ "-" | o @ "/" | o @ "*" | o @ "^" |
            o @ "rem" | o @ "mod" | o @ "rootn" | o @ "logn" => {
            if let Some(a) = s.pop_front() {
                if let Some(b) = s.pop_front() {
                    let op: Box<Fn(f64, f64) -> f64> = match o {
                        "+" => Box::new(|a, b| a+b),
                        "-" => Box::new(|a, b| a-b),
                        "/" => Box::new(|a, b| a/b),
                        "*" => Box::new(|a, b| a*b),
                        "^" => Box::new(|a, b| a.powf(b)),
                        "rem" => Box::new(|a, b| a%b),
                        "mod" => Box::new(|a, b| {
                            // Reimplementation of mod_euc
                            let r = a%b;
                            if r < 0. {
                                r + b.abs()
                            } else {
                                r
                            }
                        }),
                        "root" => Box::new(|a, b| {
                            // nth root algorithm
                            // 1000x iterations should be enough
                            let mut x: f64 = 2.; // good guess
                            for _ in 0..1000 {
                                x += (1./b) * (a/x.powf(b-1.) - x);
                            }
                            x
                        }),
                        "log" => Box::new(|a, b| a.log(b)),
                        _ => unreachable!(),
                    };
                    s.push_front(op(a, b));
                    println!("{}", op(a, b));
                    false
                } else {
                    println!("Only got one operand; pushing back to stack.");
                    s.push_front(a);
                    false
                }
            } else {
                println!("Empty stack!");
                false
            }
        },
        o @ "ln" | o @ "log2" | o @ "log" | o @ "^2" | o @ "^3" |
            o @ "sqrt" | o @ "cbrt" => {
            if let Some(n) = s.pop_front() {
                let op: Box<Fn(f64) -> f64> = match o {
                    "ln" => Box::new(|n| n.ln()),
                    "log2" => Box::new(|n| n.log2()),
                    "log10" => Box::new(|n| n.log10()),
                    "^2" => Box::new(|n| n.powi(2)),
                    "^3" => Box::new(|n| n.powi(3)),
                    "sqrt" => Box::new(|n| n.sqrt()),
                    "cbrt" => Box::new(|n| n.cbrt()),
                    _ => unreachable!(),
                };
                s.push_front(op(n));
                println!("{}", op(n));
                false
            } else {
                println!("Empty stack!");
                false
            }
        },
        o @ "pi" | o @ "e" | o @ "π" => {
            let n = match o {
                "π" | "pi" => ::std::f64::consts::PI,
                "e" => ::std::f64::consts::E,
                _ => unreachable!(),
            };
            s.push_front(n);
            false
        },
        "x" => {
            println!("Trashing last item of stack");
            s.pop_front();
            false
        },
        _ => {
            println!("Invalid!");
            false
        },
    }
}
