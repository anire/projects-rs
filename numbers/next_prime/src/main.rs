/*
 * Projects.rs
 * Filename: ./numbers/next_prime/src/main.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


//! Prime number generation
//! using the Sieve of Erathosthenes.
//! 
//! Like Fibonacci, we use a struct
//! with a custom Iterator implementation.
//! It uses an incremental version of the sieve.

use std::collections::HashMap;

/// The (incremental) Sieve of Erathosthenes.
///
/// Why use a HashMap<u64, u64> and not a Vec<u64>?
/// Because we store the last multiple of the prime
/// with the prime. This means instead of trying
/// prime % n == 0 for each prime in the Vec,
/// we try prime + last_prime != n for each prime
/// and last prime in the HashMap. (key = prime,
/// value = last prime)
///
/// So, essentially, we do a bunch of additions
/// and comparisons instead of expensive divisions
/// and modulos and comparisons, at the expense of
/// increasing the storage space required. We also
/// have to account for the cost of storing back
/// each new multiple we find, but this should be
/// faster than doing many expensive divisions.
struct Sieve {
    primes: HashMap<u64, u64>,
    n: u64,
}

impl Sieve {
    pub fn new() -> Self {
        let primes = HashMap::new();
        let n = 2;
        Self {
            primes, n,
        }
    }
}

impl Iterator for Sieve {
    type Item = u64;

    fn next(&mut self) -> Option<u64> {
        if self.n > ::std::u64::MAX {
            None
        } else {
            // Continuously increment
            // until we have found a prime.
            loop {
                let mut is_prime = true;
                // Read: for each entry (key = prime, value = last prime)
                // in the list of known primes:
                for e in self.primes.iter_mut() {
                    // If the prime + the last multiple is equal to the number:
                    if *e.0 + *e.1 == self.n {
                        // The number is our new last multiple.
                        *e.1 = self.n;
                        // The number is not prime.
                        // Because, given a coefficient x:
                        // x*p = last multiple.
                        // x*p + p = y*p or (x+1)*p, given a coefficient y.
                        // Thus, x*p + p is a multiple of p.
                        is_prime = false;
                    }
                }
                // If the number is prime, break out of the loop.
                if is_prime {
                    break;
                } else {
                    // Else, continue on to the next number.
                    self.n += 1;
                }
            }
            // Record this as a new prime, with the last multiple being itself.
            self.primes.insert(self.n, self.n);
            // Increment the number.
            self.n += 1;
            // Return the number minus 1 (the new prime)
            Some(self.n-1)
        }
    }
}

fn main() -> Result<(), Box<dyn (::std::error::Error)>> {
    let mut sieve = Sieve::new();
    let stdin = ::std::io::stdin();
    let mut stdin = stdin.lock();
    let mut inp = String::new();
    loop {
        inp.clear();
        use std::io::BufRead;
        stdin.read_line(&mut inp)?;
        if inp.as_str().starts_with("q") {
            break;
        }
        // Time it.
        let now = ::std::time::Instant::now();
        let next = sieve.next();
        let elapsed = now.elapsed();
        // Convert to seconds.
        let elapsed = elapsed.as_secs() as f64 + elapsed.subsec_nanos() as f64*1e-9;
        let next = if let Some(x) = next {
            x
        } else {
            break
        };
        println!("{} ({}s)", next, elapsed);
        use std::io::Write;
        ::std::io::stdout().flush()?;
    }
    Ok(())
}
