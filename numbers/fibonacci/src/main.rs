/*
 * Projects.rs
 * Filename: ./numbers/fibonacci/src/main.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


//! n Fibonacci numbers.
//!
//! Uses a struct with a custom Iter
//! implementation to get the numbers.
//!
//! Also has the option to get the Fibonacci
//! numbers below a certain number.

extern crate clap;

struct Fibonacci {
    a: f64,
    b: f64,
}

impl Fibonacci {
    pub fn new() -> Self {
        Fibonacci {
            a: 1.,
            b: 1.,
        }
    }
}

impl Iterator for Fibonacci {
    type Item = f64;

    fn next(&mut self) -> Option<f64> {
        use std::mem::replace;
        // That's a fancy and efficient way of saying:
        // tmp = a
        // a = b
        // b = tmp + b
        self.b += replace(&mut self.a, self.b);
        // In floating point, if it is Inf, assume oop (outofprecision)
        if self.b.is_finite() {
            Some(self.b)
        } else {
            None
        }
    }
}

fn main() -> Result<(), Box<dyn (::std::error::Error)>> {
    use clap::{App, Arg};
    let matches = App::new("fibonacci")
        .author("Anirudh Balaji <anirudhqazxswced@gmail.com>")
        .about("Calculates fibonacci numbers.")
        .arg(Arg::with_name("n")
             .help("Number; see below for how it is used.")
             .required(true)
             .takes_value(true)
             .index(1))
        .arg(Arg::with_name("first")
             .short("f")
             .long("first")
             .takes_value(false)
             .help("Take the first n Fibonacci numbers.\nThis is the default.")
             .conflicts_with("bound"))
        .arg(Arg::with_name("bound")
             .short("b")
             .long("bound")
             .takes_value(false)
             .help("Take the Fibonacci numbers under n.")
             .conflicts_with("first"))
        .get_matches();
    let n = matches.value_of("n").unwrap().parse::<usize>()?;
    let fib_n = Fibonacci::new();
    let fib_n: Box<Iterator<Item = f64>> = if matches.is_present("bound") {
        Box::new(fib_n.filter(|&x| x < n as f64)) // Less than n
    } else {
        Box::new(fib_n.take(n)) // Take first n values
    };
    println!("Note: If there is an overflow, the list will be truncated.");
    if matches.is_present("bound") {
        println!("Fibonacci numbers under {}", n);
    } else {
        println!("First {} Fibonacci numbers", n);
    }
    println!("==========================================================");
    for num in fib_n {
        println!("{}", num);
    }
    Ok(())
}
