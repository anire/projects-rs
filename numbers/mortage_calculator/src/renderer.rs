/*
 * Projects.rs
 * Filename: ./numbers/mortage_calculator/src/renderer.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


use math::*;
/* This is a fancy way of saying we
   want to use std and std::io::prelude::* */
use std::{self, io::prelude::*};
/* crate import from the prelude is experimental
   blah blah blah blah */
use super::{liner, termion};

/* Handy macro which formats something and gives us its length.
 * (Cause we do that a lot.)
 */
macro_rules! format_len {
    ($($a:expr),+) => {{
        let __ = format!($($a),+);
        let ___ = __.len();
        (__, ___)
    }}
}

/// Indicates which field we are on.
enum CursorField {
    Years,
    Principal,
    InterestRate,
}

/// Handles rendering a Data.
/// The renderer also handles positioning
/// which it sends back to the input engine.
/// (This is required to position the input field
///  properly.)
pub struct Renderer {
    screen_height: u16,
    cursor_field: CursorField,
    data: Data,
}

impl Renderer {
    pub fn new() -> std::io::Result<Self> {
        let (_, screen_height) = termion::terminal_size()?;
        Ok(Self {
            screen_height,
            cursor_field: CursorField::Years, data: Data::new(),
        })
    }

    pub fn render<W>(&mut self, scr: &mut termion::screen::AlternateScreen<W>) -> std::io::Result<()>
        where W: Write {
        let (monthly, total, years, rate, principal) = self.data.render_data();
        let num_lines = 6;
        let spacing = (self.screen_height-num_lines)/2;
        use termion::cursor::*;
        write!(scr, "{}{}{}", ::termion::clear::All, Hide, Goto(1, spacing))?;
        let (years_fmt, years_len) = format_len!("Years: {}", years);
        write!(scr, "{}\r\n", years_fmt)?;
        let principal_fmt = format!("Principal ($): {}", principal);
        let principal_len = principal_fmt.len();
        write!(scr, "{}\r\n", principal_fmt)?;
        let rate_fmt = format!("Interest Rate (%): {}", rate*100.);
        let rate_len = rate_fmt.len();
        write!(scr, "{}\r\n", rate_fmt)?;
        write!(scr, "\r\n")?; // skip line
        write!(scr, "Monthly Payment ($): {}\r\n", monthly)?;
        write!(scr, "Total Payment ($): {}\r\n", total)?;
        use self::CursorField::*;
        match self.cursor_field {
            Years => write!(scr, "{}", Goto(years_len as u16, spacing)),
            Principal => write!(scr, "{}", Goto((principal_len+1) as u16, spacing+1)),
            InterestRate => write!(scr, "{}", Goto((rate_len+1) as u16, spacing+2)),
        }?;
        write!(scr, "{}", Show)?;
        scr.flush()?;
        Ok(())
    }

    pub fn down(&mut self) {
        use self::CursorField::*;
        self.cursor_field = match self.cursor_field {
            Years => Principal,
            Principal => InterestRate,
            InterestRate => Years,
        };
    }

    pub fn up(&mut self) {
        use self::CursorField::*;
        self.cursor_field = match self.cursor_field {
            Years => InterestRate,
            Principal => Years,
            InterestRate => Principal,
        };
    }

    pub fn edit<W>(&mut self, scr: &mut termion::screen::AlternateScreen<W>) -> ::std::io::Result<()>
        where W: Write {
        use termion::cursor::*;
        use self::CursorField::*;
        let num_lines = 6;
        let spacing = (self.screen_height-num_lines)/2;
        let (_, _, years, rate, principal) = self.data.render_data();
        match self.cursor_field {
            Years => write!(scr, "{}", Goto(1, spacing)),
            Principal => write!(scr, "{}", Goto(1, spacing+1)),
            InterestRate => write!(scr, "{}", Goto(1, spacing+2)),
        }?;
        write!(scr, "{}", Show)?;
        let initial = format!("{}", match self.cursor_field {
            Years => years,
            Principal => principal,
            InterestRate => rate*100.
        });
        let prompt = match self.cursor_field {
            Years => "Years: ",
            Principal => "Principal ($): ",
            InterestRate => "Interest Rate (%): ",
        };
        scr.flush()?;
        self.render(scr)?;
        let read_in = liner::Context::new().read_line_with_init_buffer(prompt,
            &mut |_| {}, initial)?;
        let parsed = read_in.parse().unwrap_or(0.);
        match self.cursor_field {
            Years => self.data.set_years(parsed),
            Principal => self.data.set_principal(parsed),
            InterestRate => self.data.set_rate(parsed/100.),
        };
        self.data.recalculate();
        write!(scr, "{}", Hide)?;
        scr.flush()?;
        self.render(scr)?;
        Ok(())
    }
}
