/*
 * Projects.rs
 * Filename: ./numbers/mortage_calculator/src/main.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


//! Mortgage calculator written using termion
//! for a text-based interface.
//!
//! Computes mortgage using the following formula:
//! 
//! Mortgage = M
//! Principal = P
//! Interest Rate = r
//! Periods (number of payments, usually years*12) = n
//!
//! M = P(r(1+r)^n / (1+r)^n-1)
//! RPN (M=): 1r+n^r*1r+n^1-/P*
//! 
//! This is our first multi-file project!
//!
//! Note that we have to patch the liner crate
//! due a to a (possible?) bug in the crate.

extern crate termion;
extern crate liner; // For readline-like functionality

/* Handles & encapsulates all math-related stuff */
mod math;

/* Handles rendering the given data (and input fields) */
mod renderer;

/* Handles taking input */
mod input;

/// We have a custom return type that catches (most) errors.
///          vvvvvv Maybe there was an error?
///          |||||| vv Return type if successful (we have nothing meaningful to return)
///          |||||| ||  vvv Used to encapsulate types that don't have a defined size by
///          |||||| ||  ||| heap-allocating them.
///          |||||| ||  ||| vvv Used to indicate dynamic dispatch (i.e. trait objects which have no
///          |||||| ||  ||| ||| defined size)
///          |||||| ||  ||| |||  vvvvvvvvvvvvvvvvvvv The trait that most errors implement. Any
///          |||||| ||  ||| |||  ||||||||||||||||||| error which implements this trait can be
///          |||||| ||  ||| |||  ||||||||||||||||||| returned as an error from our main function.
fn main() -> Result<(), Box<dyn (::std::error::Error)>> {
    use termion::raw::IntoRawMode;
    let stdin = std::io::stdin();
    let screen = termion::screen::AlternateScreen::from(std::io::stdout().into_raw_mode()?);

    input::main_loop(screen, stdin)?;

    Ok(())
}
