/*
 * Projects.rs
 * Filename: ./numbers/mortage_calculator/src/input.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


//! Contains the main loop
//! for handling input.

pub fn main_loop<W>(mut scr: ::termion::screen::AlternateScreen<W>, stdin: ::std::io::Stdin) -> Result<(), Box<dyn (::std::error::Error)>>
    where W: ::std::io::Write {
    use termion::input::TermRead;
    use termion::event::Key;
    let mut render = super::renderer::Renderer::new()?;
    let mut needs_edit = false;
    render.render(&mut scr)?;
    for key in stdin.keys() {
        match key? {
            Key::Esc => return Ok(()),
            Key::Down => render.down(),
            Key::Up => render.up(),
            Key::Char('\n') => needs_edit = true,
            _ => {},
        }
        render.render(&mut scr)?;
        if needs_edit {
            render.edit(&mut scr)?;
            needs_edit = false;
        }
    }
    Ok(())
}
