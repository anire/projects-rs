/*
 * Projects.rs
 * Filename: ./numbers/prime_factorization/src/main.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


//! Prime factorization using
//! trial division.
//!
//! This is the worst possible algorithm.

extern crate clap;

fn factorize(mut n: u64) -> Vec<u64> {
    let mut factors = Vec::new();
    let mut i = 2;
    while n > 1 {
        if n%i == 0 {
            factors.push(i);
            n /= i;
        } else {
            i += 1;
        }
    }
    factors
}

fn main() -> Result<(), Box<dyn (::std::error::Error)>> {
    use clap::{App, Arg};
    let matches = App::new("prime_factorization")
        .author("Anirudh Balaji <anirudhqazxswced@gmail.com>")
        .about("Calculates the prime factorization of a number.")
        .arg(Arg::with_name("n")
             .takes_value(true)
             .index(1)
             .required(true)
             .help("The number to factorize."))
        .get_matches();
    let n = matches.value_of("n").unwrap().parse()?;
    let factors = factorize(n);
    println!("Factors of {}: {:?}", n, factors);
    Ok(())
}
