/*
 * Projects.rs
 * Filename: ./numbers/binary_converter/src/main.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


//! Binary <-> Decimal converter.
//! Converts to decimal using u64::from_str_radix,
//! converts to binary using {:b} (binary format)

extern crate clap;

fn main() -> Result<(), Box<dyn (::std::error::Error)>> {
    use clap::{App, Arg};
    let matches = App::new("binary_converter")
        .author("Anirudh Balaji <anirudhqazxswced@gmail.com>")
        .about("Converts between binary and decimal.")
        .arg(Arg::with_name("binary")
             .help("If set, converts to binary. The default.")
             .takes_value(false)
             .short("b")
             .long("binary")
             .conflicts_with("decimal"))
        .arg(Arg::with_name("decimal")
             .help("If set, converts to decimal. Conflicts with -b.")
             .takes_value(false)
             .short("d")
             .long("decimal")
             .conflicts_with("binary"))
        .arg(Arg::with_name("number")
             .help("The number to operate on.")
             .required(true)
             .takes_value(true)
             .index(1))
        .get_matches();
    let num = matches.value_of("number").unwrap();
    if matches.is_present("decimal") {
        println!("Decimal: {}", u64::from_str_radix(&num, 2)?);
    } else {
        println!("Binary: {:b}", num.parse::<u64>()?);
    }
    Ok(())
}
