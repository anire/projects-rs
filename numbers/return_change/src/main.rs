/*
 * Projects.rs
 * Filename: ./numbers/return_change/src/main.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


//! Change calculator.
//!
//! Takes the amount to pay,
//! how much you give them
//! and calculates the change you get back.
//!
//! Uses an iterative algorithm. (probably the only one)
//! 
//! We display our Change using a custom fmt::Display implementation
//! on our Change struct, whose constructor is responsible for
//! calculating the "change" of a money amount. (like how many dimes, nickels, etc.)

extern crate clap;

/// Contains all dollar bill constants
/// (which have the value of the respective bill)
/// and coin constants.
mod money {
    pub const ONE_DOLLAR: f64 = 1f64;
    pub const TWO_DOLLAR: f64 = 2f64;
    pub const FIVE_DOLLAR: f64 = 5f64;
    pub const TEN_DOLLAR: f64 = 10f64;
    pub const TWENTY_DOLLAR: f64 = 20f64;
    pub const FIFTY_DOLLAR: f64 = 50f64;
    pub const HUNDRED_DOLLAR: f64 = 100f64;

    pub const PENNY: f64 = 0.01;
    pub const NICKEL: f64 = 0.05;
    pub const DIME: f64 = 0.10;
    pub const QUARTER: f64 = 0.25;
}

/// Helper function that checks if a float is
/// not zero (i.e. > the epsilon)
fn not_zero(n: f64) -> bool {
    n > ::std::f64::EPSILON
}

/// Contains the number of each money item
/// to give as change.
///
/// Its constructor converts a money amount
/// to the amount of each money item.
struct Change {
    pennies: f64,
    nickels: f64,
    dimes: f64,
    quarters: f64,
    ones: f64,
    twos: f64,
    fives: f64,
    tens: f64,
    twenties: f64,
    fifties: f64,
    hundreds: f64,
}

impl Change {
    pub fn new(mut amount: f64) -> Self {
        let mut pennies = 0f64;
        let mut nickels = 0f64;
        let mut dimes = 0f64;
        let mut quarters = 0f64;
        let mut ones = 0f64;
        let mut twos = 0f64;
        let mut fives = 0f64;
        let mut tens = 0f64;
        let mut twenties = 0f64;
        let mut fifties = 0f64;
        let mut hundreds = 0f64;
        /*
         * Start with the biggest first.
         * Hundres, fifties, twenties, etc.
         */
        use money::*;
        while amount >= HUNDRED_DOLLAR {
            hundreds += 1f64;
            amount -= HUNDRED_DOLLAR;
        }
        while amount >= FIFTY_DOLLAR {
            fifties += 1f64;
            amount -= FIFTY_DOLLAR;
        }
        while amount >= TWENTY_DOLLAR {
            twenties += 1f64;
            amount -= TWENTY_DOLLAR;
        }
        while amount >= TEN_DOLLAR {
            tens += 1f64;
            amount -= TEN_DOLLAR;
        }
        while amount >= FIVE_DOLLAR {
            fives += 1f64;
            amount -= FIVE_DOLLAR;
        }
        while amount >= TWO_DOLLAR {
            twos += 1f64;
            amount -= TWO_DOLLAR;
        }
        while amount >= ONE_DOLLAR {
            ones += 1f64;
            amount -= ONE_DOLLAR;
        }
        while amount >= QUARTER {
            quarters += 1f64;
            amount -= QUARTER;
        }
        while amount >= DIME {
            dimes += 1f64;
            amount -= DIME;
        }
        while amount >= NICKEL {
            nickels += 1f64;
            amount -= NICKEL;
        }
        pennies = (amount / PENNY).round();
        Self { pennies, nickels, dimes, quarters, ones,
        twos, fives, tens, twenties, fifties, hundreds }
    }
}

impl ::std::fmt::Display for Change {
    fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        use money::*;
        let total = (self.pennies * PENNY) + (self.nickels * NICKEL) + (self.dimes * DIME) +
            (self.quarters * QUARTER) + (self.ones * ONE_DOLLAR) + (self.twos * TWO_DOLLAR) +
            (self.fives * FIVE_DOLLAR) + (self.tens * TEN_DOLLAR) + (self.twenties * TWENTY_DOLLAR) +
            (self.fifties * FIFTY_DOLLAR) + (self.hundreds * HUNDRED_DOLLAR);
        writeln!(fmt, "${:.2}, which is:", total)?;
        if not_zero(self.hundreds) {
            writeln!(fmt, "{} hundreds", self.hundreds)?;
        }
        if not_zero(self.fifties) {
            writeln!(fmt, "{} fifties", self.fifties)?;
        }
        if not_zero(self.twenties) {
            writeln!(fmt, "{} twenties", self.twenties)?;
        }
        if not_zero(self.tens) {
            writeln!(fmt, "{} tens", self.tens)?;
        }
        if not_zero(self.fives) {
            writeln!(fmt, "{} fives", self.fives)?;
        }
        if not_zero(self.twos) {
            writeln!(fmt, "{} twos", self.twos)?;
        }
        if not_zero(self.ones) {
            writeln!(fmt, "{} ones", self.ones)?;
        }
        if not_zero(self.quarters) {
            writeln!(fmt, "{} quarters", self.quarters)?;
        }
        if not_zero(self.dimes) {
            writeln!(fmt, "{} dimes", self.dimes)?;
        }
        if not_zero(self.nickels) {
            writeln!(fmt, "{} nickels", self.nickels)?;
        }
        if not_zero(self.pennies) {
            write!(fmt, "{} pennies", self.pennies)
        } else {
            Ok(())
        }
    }
}

fn main() -> Result<(), Box<dyn (::std::error::Error)>> {
    use clap::{App, Arg};
    let matches = App::new("return_change")
        .author("Anirudh Balaji <anirudhqazxswced@gmail.com>")
        .about("Calculates change given the amount to pay and the amount given.")
        .arg(Arg::with_name("amount")
             .required(true)
             .help("The amount to pay.")
             .index(1))
        .arg(Arg::with_name("given")
             .required(true)
             .help("The amount given.")
             .index(2))
        .get_matches();
    let amt = matches.value_of("amount").unwrap().parse::<f64>()?;
    let given = matches.value_of("given").unwrap().parse::<f64>()?;
    let diff = given - amt;
    let change = Change::new(diff);
    println!("Change: {}", change);
    Ok(())
}
