/*
 * Projects.rs
 * Filename: ./numbers/fast_exponentiation/src/main.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * Fast Exponentiation
 * aka Exponentiation by Squaring
 * aka Binary Exponentiation
 *
 * Quoting Wikipedia:
 * This method is based on the observation that, for a positive integer n:
 * x^n = {
 *   x(x^2)^((n-1)/2) if n is odd
 *   (x^2)^(n/2) if n is even
 * }
 */
fn fast_exp(x: u64, n: u64) -> u64 {
    /* We will use binary exponentiation */
    // Find highest bit of n
    let mut highest_bit = 0;
    while n > (1 << highest_bit) {
        highest_bit += 1;
    }
    // Iterate over each bit, starting from the highest
    let mut r = 1; // = x^0
    for bit in (0..=highest_bit).rev() {
        r *= r; // in either step, x^2
        // if odd, then *=x again
        if (n & (1 << bit)) > 0 {
            r *= x;
        }
        // as we go to the next iteration, the value of each of the bits before
        // becomes twice their original value (base 2)
    }
    r
}

extern crate clap;

fn main() -> Result<(), Box<dyn (::std::error::Error)>> {
    use clap::{Arg, App};
    let matches = App::new("fast_exponentiation")
        .author("Anirudh Balaji <anirudhqazxswced@gmail.com>")
        .about("Performs binary exponentiation on two positive integers in O(log n) time.")
        .arg(Arg::with_name("base")
             .help("The base.")
             .takes_value(true)
             .index(1)
             .required(true))
        .arg(Arg::with_name("exponent")
             .help("The exponent.")
             .takes_value(true)
             .index(2)
             .required(true))
        .get_matches();
    let a = matches.value_of("base").unwrap().parse()?;
    let b = matches.value_of("exponent").unwrap().parse()?;
    println!("{}^{} = {}", a, b, fast_exp(a, b));
    Ok(())
}
