/*
 * Projects.rs
 * Filename: ./numbers/alarm_clock/src/bin/client.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* The client for the alarm clock. */
/**
 * Serde: The standard SERialization/DEserialization library
 */
extern crate serde;
/**
 * Serde_derive: Derive Serialize/Deserialize implementation on your object.
 */
#[macro_use]
extern crate serde_derive;
/**
 * Serde_json: JSON support for Serde
 */
extern crate serde_json;
/**
 * Chrono: The standard time library
 */
extern crate chrono;
/**
 * Clap: The Command-Line Argument Parser
 */
extern crate clap;

/*Standard includes*/use std::{
    os::unix::net::UnixStream,
    time::Duration,
};

/*Clap includes*/use clap::{App, Arg};

const SOCK_FILE: &'static str = "music.sock";

/* Common definitions between client/server,
 * too lazy to modularize */

#[derive(Serialize, Deserialize)]
struct Response {
    err: Option<String>,
    ok: bool,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(tag = "type")]
enum Request {
    SecondCountdown {
        seconds: u64,
    },
    TimeCountdown {
        year: i32,
        month: u32,
        day: u32,
        hours: u32, /* 24-hour time */
        minutes: u32,
        seconds: u32,
    },
}

/* Internal struct passed to generate_request. */
#[derive(Default)]
struct GenerateRequest<'a> {
    wait_until: bool,
    maybe_years: Option<&'a str>,
    maybe_months: Option<&'a str>,
    maybe_days: Option<&'a str>,
    maybe_hours: Option<&'a str>,
    maybe_minutes: Option<&'a str>,
    maybe_seconds: Option<&'a str>
}

/* Number validator */
fn number_validator(v: String) -> Result<(), String> {
    v
        .parse::<u32>()
        .map(|_| ())
        .map_err(|e| format!("Not a number! {}", e))
}

fn main() {
    let matches = App::new("alarm-clock")
        .arg(Arg::with_name("years")
             .long("years")
             .takes_value(true)
             .help(
                 "The number of years. Uncommonly used, and thus put behind a long flag."
             )
             .validator(number_validator))
        .arg(Arg::with_name("months")
             .long("months")
             .takes_value(true)
             .help(
                 "The number of months. Uncommonly used, and thus put behind a long flag."
             )
             .validator(number_validator))
        .arg(Arg::with_name("hours")
             .short("h")
             .long("hours")
             .takes_value(true)
             .help("The number of hours.")
             .validator(number_validator))
        .arg(Arg::with_name("minutes")
             .short("m")
             .long("minutes")
             .takes_value(true)
             .help("The number of minutes.")
             .validator(number_validator))
        .arg(Arg::with_name("seconds")
             .short("s")
             .long("seconds")
             .takes_value(true)
             .required_unless_one(&["years", "months", "hours", "minutes"])
             .help("The number of seconds")
             .validator(number_validator))
        .arg(Arg::with_name("until")
             .short("u")
             .long("until")
             .help("Flag to wait \"until\" the time specified _today_.")
             .conflicts_with("for"))
        .arg(Arg::with_name("for")
             .short("f")
             .long("for")
             .help("Flag to wait for the time specified. (Default)")
             .conflicts_with("until"))
        .get_matches();
    use std::io::prelude::*;
    let mut gen_req = GenerateRequest::default();
    gen_req.wait_until = matches.is_present("until");
    gen_req.maybe_years = matches.value_of("years");
    gen_req.maybe_months = matches.value_of("months");
    gen_req.maybe_days = matches.value_of("days");
    gen_req.maybe_hours = matches.value_of("hours");
    gen_req.maybe_minutes = matches.value_of("minutes");
    gen_req.maybe_seconds = matches.value_of("seconds");
    let req = generate_request(gen_req);
    let serialized = serde_json::to_string(&req).unwrap();
    let mut stream = UnixStream::connect(SOCK_FILE).unwrap();
    stream.write_all(serialized.as_bytes()).unwrap();
    stream.shutdown(::std::net::Shutdown::Write).unwrap();
    let resp: Response = serde_json::from_reader(stream).unwrap();
    if resp.ok {
        println!("OK");
    } else {
        println!("err: {}", resp.err.unwrap());
    }
}

/**
 * Generates a Request
 * based on the given parameters.
 */
fn generate_request<'a>(params: GenerateRequest<'a>) -> Request {
    let today = chrono::Local::today();
    let years = params.maybe_years.unwrap_or("0");
    let months = params.maybe_months.unwrap_or("0");
    let days = params.maybe_days.unwrap_or("0");
    let hours = params.maybe_hours.unwrap_or("0");
    let minutes = params.maybe_minutes.unwrap_or("0");
    let seconds = params.maybe_seconds.unwrap_or("0");
    /* Safe to parse because of validator */
    let years = years.parse().unwrap();
    let months = months.parse().unwrap();
    let days = days.parse().unwrap();
    let hours = hours.parse().unwrap();
    let minutes = minutes.parse().unwrap();
    let seconds = seconds.parse().unwrap();
    use chrono::prelude::*;
    // Create Request based on parameters
    if params.wait_until {
        Request::TimeCountdown {
            year: if years == 0 {
                today.year()
            } else {
                years
            },
            month: if months == 0 {
                today.month()
            } else {
                months
            },
            day: if days == 0 {
                today.day()
            } else {
                days
            },
            hours: hours,
            minutes: minutes,
            seconds: seconds,
        }
    } else {
        Request::SecondCountdown {
            seconds:
                (years as u64 * /* seconds per year */31557600) +
                (months as u64 * /* seconds per month */2629800) +
                (days as u64 * /* seconds per day */86400) +
                (hours as u64 * /* seconds per hour */3600) +
                (minutes as u64 * /* seconds per minute */60) +
                (seconds as u64),
        }
    }
}
