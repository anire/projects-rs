/*
 * Projects.rs
 * Filename: ./numbers/alarm_clock/src/timekeeper.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The time-keeping thread.
 * Receives new countdowns using a Arc<Mutex<Option<Duration>>>, and decrements
 * the countdowns in one-second intervals, sending a message to the audio thread
 * if necessary.
 */

use std::thread;
use std::time::Duration;
use std::sync::{Arc, Mutex, atomic};

pub fn timekeeper_thread_start(abool: Arc<atomic::AtomicBool>)
    -> (thread::JoinHandle<()>, Arc<Mutex<Option<Duration>>>) {
    let arc_mutex: Arc<Mutex<Option<Duration>>> = Arc::new(Mutex::new(None));
    let thread_copy = arc_mutex.clone();
    let hand = thread::spawn(move || {
        let mut countdowns: Vec<Duration> = Vec::new();
        loop {
            { /* BEGIN: Scope where Mutex is locked */
                let mut locked = thread_copy.lock().unwrap();
                if locked.is_some() {
                    // Take and add to countdowns.
                    countdowns.push(locked.take().unwrap());
                }
            } /* END: Scope where Mutex is locked */

            let mut will_signal = false;
            for dur in countdowns.iter_mut() {
                if dur.as_secs() == 0 {
                    will_signal = true;
                } else {
                    *dur = Duration::from_secs(dur.as_secs()-1);
                    if dur.as_secs() == 0 {
                        will_signal = true;
                    }
                }
            }
            if will_signal {
                // Remove zero durations from vector
                countdowns = countdowns
                    .into_iter()
                    .filter(|x| x.as_secs() != 0)
                    .collect();
                // Signal to audio thread using received AtomicBool
                abool.store(true, atomic::Ordering::SeqCst);
            }
            // Sleep one second
            thread::sleep(Duration::from_secs(1));
        }
    });
    (hand, arc_mutex)
}
