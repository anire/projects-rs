/*
 * Projects.rs
 * Filename: ./numbers/alarm_clock/src/main.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * Audrey - Audio file reading
 */
extern crate audrey;
/**
 * Cpal - Cross Platform Audio Library
 */
extern crate cpal;
/**
 * Sample - sample conversion, u(audrey)
 */
extern crate sample;
/**
 * Tokio - fast, asynchronous I/O
 */
extern crate tokio;
/**
 * Tokio_uds - Unix Domain socket for Tokio
 */
extern crate tokio_uds;
/**
 * Serde - the standard SERialization/DEserialization library
 */
extern crate serde;
/**
 * Derive Serialize/Deserialize implementations for structs automatically
 */
#[macro_use]
extern crate serde_derive;
/**
 * Serde_json - JSON support for Serde
 */
extern crate serde_json;
/**
 * Chrono - the standard time library
 */
extern crate chrono;

use tokio::prelude::*;
use tokio_uds::UnixListener;

mod audio_handler;
mod timekeeper;

const UNIX_SOCKET: &'static str = "music.sock";

/* Converts an error to a boxed trait object. */
fn errorase<E>(e: E) -> Box<::std::error::Error + Send>
    where E: 'static + ::std::error::Error + Send {
    Box::new(e)
}

/**
 * Wait for an Arc<Mutex<Option<T>>> to become None,
 * then fill it with the provided object.
 */
fn arc_queue<T>(a: &::std::sync::Arc<::std::sync::Mutex<Option<T>>>, v: T) {
    // Continously lock/unlock until it is None.
    while a.lock().unwrap().is_some() {}
    // Change the value without dereferencing it.
    a.lock().unwrap().get_or_insert(v);
}

/* Represents a request.
 * Can either be to add a second countdown or wait until a specific time (which
 * is converted into a second countdown anyways)
 */
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(tag = "type")]
enum Request {
    SecondCountdown {
        seconds: u64,
    },
    TimeCountdown {
        year: i32,
        month: u32,
        day: u32,
        hours: u32, /* 24-hour time */
        minutes: u32,
        seconds: u32,
    },
}

/* Represents a response to a request.
 * If the field err is present and ok is false, the error should be handled.
 * If ok is true, the request should be considered successful.
 */
#[derive(Serialize, Deserialize)]
struct Response {
    err: Option<String>,
    ok: bool,
}

fn main() -> Result<(), Box<dyn (::std::error::Error)>> {
    let (_, abool) = audio_handler::audio_thread_start();
    let (_, tk_arc) = timekeeper::timekeeper_thread_start(abool);
    // Remove previous socket-file before opening socket
    ::std::fs::remove_file(UNIX_SOCKET);

    // Open socket and run server task
    let listener = UnixListener::bind(UNIX_SOCKET)?;

    let server = listener.incoming()
        .for_each(move |sock| {
            // clone arc for usage
            let tk_arc = tk_arc.clone();
            // split into reader/writer
            let (rdr, wtr) = sock.split();
            // Parse into a Request enum.
            let future = tokio::io::read_to_end(rdr, Vec::new())
                .map_err(errorase)
                .and_then(|(_, bytes)| {
                    serde_json::from_slice(&bytes).map_err(errorase)
                })
                .and_then(|req| match req {
                    Request::SecondCountdown { seconds } => {
                        Ok(::std::time::Duration::from_secs(seconds))
                    },
                    Request::TimeCountdown { year, month, day, hours, minutes, seconds } => {
                        use chrono::SubsecRound;
                        use chrono::TimeZone;
                        // Round now to the nearest second.
                        let now = chrono::Local::now().round_subsecs(0);
                        // Subtract times and return that duration.
                        let then = chrono::Local.ymd(year, month, day)
                            .and_hms(hours, minutes, seconds);
                        then.signed_duration_since(now).to_std().map_err(errorase)
                    },
                })
                .and_then(move |dur| {
                    arc_queue(&tk_arc, dur);
                    Ok(())
                })
                .then(move |res| {
                    let response = match res {
                        Ok(()) => Response {
                            ok: true,
                            err: None,
                        },
                        Err(e) => Response {
                            ok: false,
                            err: Some(e.to_string()),
                        },
                    };
                    let serialized = serde_json::to_string(&response).unwrap();
                    let write_fut = tokio::io::write_all(wtr, serialized);
                    write_fut
                })
                .map(|_| ())
                .map_err(|e| println!("Writing error: {:?}", e));
            tokio::spawn(future);
            Ok(())
        })
        .map_err(|e| {
            println!("Server error: {:?}", e);
        });

    tokio::run(server);

    Ok(())
}
