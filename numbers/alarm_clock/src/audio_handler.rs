/*
 * Projects.rs
 * Filename: ./numbers/alarm_clock/src/audio_handler.rs
 * Copyright (C) 2018 Anirudh Balaji 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Handles playing audio.
 * Starts a new thread which receives messages from a channel to play/stop,
 * because the event loop cannot be stopped once it is started. It reads from a
 * predefined file for the alarm audio file.
 *
 * Audrey is used to decode the audio file. On the fly, we convert the (by
 * default) f32 samples into the appropriate format and play it.
 */
use std::thread;
use std::sync::{self, atomic};

use cpal::{
    EventLoop,
    StreamData,
    UnknownTypeOutputBuffer,
    default_output_device,
};

use sample::conv::f32::{to_u16, to_i16};

const AUDIO_FILE: &'static str = "alarm.wav";

/**
 * Starts the audio thread and returns an Rc<AtomicBool>
 * Setting the boolean to true will provoke the audio thread to start playing
 * the track. While it is playing, it will ignore the state of the boolean.
 */
pub fn audio_thread_start() -> (thread::JoinHandle<()>, sync::Arc<atomic::AtomicBool>) {
    let arc_bool = sync::Arc::new(atomic::AtomicBool::new(false));
    let thread_copy = arc_bool.clone();
    let hand = thread::spawn(move || {
        /*
         * Note: the reason we unwrap all the time here is because if something
         * fails, the panic will be printed, we get a backtrace, and the audio
         * thread gracefully fails, without catastrophic effects.
         */

        let mut rdr = ::audrey::open(AUDIO_FILE).unwrap();
        let samples = rdr.samples::<f32>()
            .filter(|x| !x.is_err() || {println!("x is err: {:?}", x); false})
            .map(|x| x.unwrap())
            .collect::<Vec<_>>();
        let samples_index_max = samples.len()-1;
        let mut samples_index = 0;
        let mut playing = false;
        
        let ev = EventLoop::new();
        let device = default_output_device().unwrap();
        let format = device.default_output_format().unwrap();
        let stream = ev.build_output_stream(&device, &format).unwrap();
        ev.play_stream(stream);
        /* Begin the event loop */
        ev.run(move |/* Only playing to one stream */ _id, data| {
            /* If the previous value was true, swap it with false and return the previous value,
             * which, if the values were swapped, would be true, thus satisfying the if condition.
             */
            if thread_copy.compare_and_swap(true, false, atomic::Ordering::SeqCst) && !playing {
                playing = true;
                samples_index = 0;
            }
            match data {
                StreamData::Output { buffer: UnknownTypeOutputBuffer::U16(mut buf) } => {
                    for elem in buf.iter_mut() {
                        if playing && (samples_index < samples_index_max) {
                            let samp = samples[samples_index];
                            // Convert sample
                            *elem = to_u16(samp);
                            samples_index += 1;
                        } else {
                            /* Stop playing, we've exceeded the limit */
                            playing = false;
                            /* Default "zero" value of u16 */
                            *elem = ::std::u16::MAX / 2;
                        }
                    }
                },
                StreamData::Output { buffer: UnknownTypeOutputBuffer::I16(mut buf) } => {
                    for elem in buf.iter_mut() {
                        if playing && (samples_index < samples_index_max) {
                            let samp = samples[samples_index];
                            // Convert sample
                            *elem = to_i16(samp);
                            samples_index += 1;
                        } else {
                            /* Stop playing, we've exceeded the limit */
                            playing = false;
                            /* Default "zero" value of i16 */
                            *elem = 0;
                        }
                    }
                },
                StreamData::Output { buffer: UnknownTypeOutputBuffer::F32(mut buf) } => {
                    for elem in buf.iter_mut() {
                        if playing && (samples_index < samples_index_max) {
                            let samp = samples[samples_index];
                            *elem = samp;
                            samples_index += 1;
                        } else {
                            /* Stop playing, we've exceeded the limit */
                            playing = false;
                            /* Default "zero" value of f32 */
                            *elem = 0.0;
                        }
                    }
                },
                _ => {},
            }
        });
    });
    (hand, arc_bool)
}
