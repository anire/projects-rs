# Projects.rs
_An attempt to finish all projects on [this list][kprojects] using idiomatic Rust and crates._
If you have a suggestion or improvement (regarding code style, too), feel free to open an issue or PR!
PRs are only for improvements.

**Do not submit a solution to a project in a PR.
It will be declined because I will solve all projects (this is a learning experience for me, despite knowing
Rust intermediately.)**

[LICENSE][license]
Liner is licensed under MIT. See [their license terms][liner-license]

# Projects

These are in no specific order.

## Numbers

- [x] Approximation of π
  Notes: not much precision
- [x] Approximation of e
  Notes: not much precision
- [x] Next prime number
  Notes: uses an incremental Sieve of Erathosthenes
- [x] Prime factorization of a number
  Notes: uses a very inefficient trial-division algorithm
- [x] Cost of tile to cover W x H floor
- [x] Fibonacci sequence
- [x] Mortgage calculator
  Notes: text-based UI, currently a patch is needed to a library due to a bug
- [x] Change calculator
- [x] Binary <-> Decimal converter
- [x] Calculator
  Notes: RPN (Reverse polish notation) calculator
- [ ] Unit Converter
- [x] Alarm Clock
  Notes: From here on, all notes are in the commit messages.
- [ ] Distance between two cities
- [ ] Credit card validator
- [ ] Tax calculator
- [ ] Factorial finder
- [ ] Complex number algebra
- [ ] Happy numbers
- [ ] Number names
- [ ] Coin flip simulation
- [ ] Limit calculator
- [ ] Fast exponentiation

## Classic Algorithms

- [ ] Collatz Conjecture
- [ ] Sorting (Merge & Bubble Sort)
- [ ] Closest pair problem
- [ ] Sieve of Erathosthenes

## Graph

- [ ] Graph from links
- [ ] Eulerian path
- [ ] Graph connected? (Orig title: Connected Graph)
- [ ] Dijkstra's algorithm
- [ ] Minimum spanning tree

## Data Structures

- [ ] Inverted index

## Text

- [ ] FizzBuzz
- [ ] Reverse a string
- [ ] Pig latin
- [ ] Count vowels
- [ ] Check if palindrom
- [ ] Count words in a string
- [ ] Text editor
- [ ] RSS client (Orig title: RSS Feed Creator)
- [ ] Quote tracker
- [ ] Guestbook / Journal
- [ ] Vignere / Vernam / Caesar ciphers
- [ ] Regex query tool

  Note: well known tool is [Regexr][regexr]

## Networking

- [ ] FTP client (Orig title: FTP Program)
- [ ] Bandwidth monitor
- [ ] Port scanner
- [ ] Mail checker
- [ ] Country from IP lookup
- [ ] Whois search tool
- [ ] Site checker with time scheduling

## Classes

- [ ] Product inventory
- [ ] Airline / Hotel reservation system
- [ ] Company manager
- [ ] Bank account manager
- [ ] Patient / Doctor scheduler
- [ ] Recipe creator and manager
- [ ] Image gallery
- [ ] Shape classes with area & perimeter (Orig title: Shape Area and Perimeter Classes)

  Note: in Rust this would be done using a Shape trait
- [ ] Flower shop order
- [ ] Family tree creator

## Threading

- [ ] Progress bar for downloads
- [ ] Bulk thumbnail creator

## Web

- [ ] Page scraper
- [ ] Online whiteboard
- [ ] Atomic time from internet API (Orig title: Get Atomic Time from Internet Clock)
- [ ] Fetch weather
- [ ] Automatic login & action
- [ ] E-Card Generator
- [ ] Content Management System (CMS)
- [ ] Web Board (Forum)
- [ ] CAPTCHA maker

## Files

- [ ] Quiz maker
- [ ] Excel/CSV sorter
- [ ] Zip file maker
- [ ] PDF maker
- [ ] MP3 tagger
- [ ] Code snippet manager

  Note: [Dash][dash] contains one. Good for reference!

## Databases

- [ ] SQL query analyzer
- [ ] Remote SQL tool
- [ ] Report generator
- [ ] Event scheduler/calendar
- [ ] Budget tracker
- [ ] TV show tracker
- [ ] Travel Planner

## Graphics and Multimedia

- [ ] Slideshow player
- [ ] Video streamer
- [ ] MP3 player
- [ ] Watermarking application
- [ ] Turtle graphics
- [ ] GIF maker

Note that the Security section has been left out because it duplicates
a previous project.

[kprojects]: https://github.com/karan/Projects
[regexr]: https://regexr.com
[dash]: https://kapeli.com/dash
[license]: https://gitlab.com/anire/projects-rs/blob/master/LICENSE
[liner-license]: https://github.com/MovingtoMars/liner/blob/master/LICENSE
